<?php
return [
	'app' => [
		'url'	=> 'http://myauth.dev',
		'hash'	=> [
			'algo' => PASSWORD_BCRYPT,
			'cost'  => 10
		]
	],
	'db' => [
		'driver' 	=> 'mysql',
		'host' 		=> 'localhost',
		'name' 		=> 'myauth',
		'username' 	=> 'root',
		'password' => 'root',
		'charset'	=> 'utf8',
		'collation'	=> 'utf8_unicode_ci'
	],
	'auth' => [
		'session'	=> 'user_id',
		'remember'	=> 'user_rem'
	],
	'mail'	=> [
		'smtp_auth'		 => true,
		'smtp_secure'	 => 'tls',
		'host'			 => '',
		'username'		 => '',
		'password'		 => '',
		'port'			 => 587,
		'html'			 => true
	],
	'twig'	=>[
		'debug' => true,
	],
	'csrf'	=> [
		'key' =>	'csrf_token',
	]
];