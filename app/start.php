<?php
/*
*	NameSpaced Classes Slim / Twig
*/
use Slim\Slim;
use Slim\Views\Twig;
use Slim\Views\TwigExtension;

use Noodlehaus\Config;
use RandomLib\Factory as RandomLib;

/*
*	NameSpaced Classes Custom
*/
use Tjrc\User\User;
use Tjrc\User\UserPermissions;
use Tjrc\Helpers\Hash;
use Tjrc\Validation\Validator;
use Tjrc\Middleware\BeforeMiddleware;
use Tjrc\Middleware\CsrfMiddleware;
use Tjrc\Mail\Message;
use Tjrc\Mail\Mailer;

/*
*	Start Session
*/
session_cache_limiter(false);
session_start();

/*
*	Only for Development Purposes.
*/
ini_set('display_errors', 'On');


/*
*	Define Include Root / get Autoloader.
*	Will autoload in all classes and allow us to use namespacing
*/
define('INC_ROOT', dirname(__DIR__));
require INC_ROOT . '/vendor/autoload.php';


/*
*	 Creates new Slim instance,
*	 Sets some defaults for  Mode / Views / Twig
*/
$app = new Slim([
	'mode' => file_get_contents(INC_ROOT .'/mode.php'),
	'view'	=> new Twig(),
	'templates.path' => INC_ROOT . '/app/views'
]);


/*
*	Before Middleware
*/
$app->add(new BeforeMiddleware);
$app->add(new CsrfMiddleware);


/*
*	Configure App "Production" or "Developement" mode (loads config)
*/
$app->configureMode($app->config('mode'), function() use ($app){
	$app->config = Config::load(INC_ROOT . "/app/config/{$app->mode}.php");
});



/*
*	Require db connection & included routes
*/
require 'database.php';
require 'filters.php';
require 'routes.php';


/*
*	Presets
*/
$app->auth = false;

/*
*	Instansiate All Classes into Container
*	Accessible in routes as $app->{class name};
*/
$app->container->set('user', function(){
	return new User;
});

$app->container->set('userpermissions', function(){
	return new UserPermissions;
});

/*
*	Instansiate Hash into $app container
*/
$app->container->singleton('hash', function() use ($app){
	return new Hash($app->config);
});


$app->container->singleton('randomlib', function(){
	$factory = new RandomLib;
	return $factory->getMediumStrengthGenerator();
});



/*
*	Instansiate Violin Into $app container
*/
$app->container->singleton('validation', function() use ($app) {
	return new Validator($app->user);
});

$app->container->singleton('mail', function() use ($app){

    $mailer = new PHPMailer;

    $mailer->Host 			= $app->config->get('mail.host');
    $mailer->SMTPAuth 		= $app->config->get('mail.smtp_auth');
    $mailer->SMTPSecure 	= $app->config->get('mail.smtp_secure');
    $mailer->Port 			= $app->config->get('mail.port');
    $mailer->Username 		= $app->config->get('mail.username');
    $mailer->Password 		= $app->config->get('mail.password');
    $mailer->isHTML($app->config->get('mail.html'));

    return new Mailer($app->view, $mailer);

});


/*
*	Create Slim View Var & Set Options to View.
*/

$view = $app->view();


$view->parserOptions = [
	'debug' => $app->config->get('twig.debug')
];

$view->parserExtensions = [
	new TwigExtension
];



