<?php
	/***
	*  Add all routes here as you create them.
	***/
	require INC_ROOT . '/app/routes/home.php';

	/* Auth */
	require INC_ROOT . '/app/routes/auth/register.php';
	require INC_ROOT . '/app/routes/auth/login.php';
	require INC_ROOT . '/app/routes/auth/activate.php';
	require INC_ROOT . '/app/routes/auth/logout.php';
	require INC_ROOT . '/app/routes/auth/password/recover.php';
	require INC_ROOT . '/app/routes/auth/password/update.php';
	require INC_ROOT . '/app/routes/auth/password/reset.php';


	/* Dashboard */
	require INC_ROOT . '/app/routes/dashboard/dashboard.php';

	/*Edit Users */
	require INC_ROOT . '/app/routes/users/list_users.php';
	require INC_ROOT . '/app/routes/users/edit_users.php';
	require INC_ROOT . '/app/routes/users/edit_profile.php';