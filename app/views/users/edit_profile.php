{%  extends 'templates/default.php' %}

{% block content %}

	<a href="{{ urlFor('dashboard') }}"> << Back to Dashboard</a>

	<h3>You're Editing, {{auth.getFullNameOrUsername() }}</h3>


	<h4><strong>Edit Profile Information</strong></h4>
	<form action="{{ urlFor('users.edit.profile', {profile: auth.username}) }}" method="post">
		<div>
			<input type="text" name="firstname" placeholder="Update First Name" {% if request.post('firstname') %} value="{{ request.post('firstname') }}" {%else%}  value="{{ auth.first_name }}" {%endif%}>
			{% if errors.first('firstname') %}  {{ errors.first('firstname') }} {% endif %}
		</div>
		<div>
			<input type="text" name="lastname" placeholder="Update Last Name" {% if request.post('lastname') %} value="{{ request.post('lastname') }}" {%else%}  value="{{ auth.last_name }}" {%endif%}>
			{% if errors.first('lastname') %}  {{ errors.first('lastname') }} {% endif %}
		</div>
		<input type="hidden" name="{{ csrf_key }}" id="submit" value="{{ csrf_token }}">
		<button name="submit">Update Name</button>
	</form>



	<h4><strong>Reset Password</strong></h4>
	<form action="{{ urlFor('users.edit.profile', {profile: auth.username}) }}" method="post">
		<div>
			<input type="password" name="password" placeholder="New Password">
			{% if errors.first('password') %}  {{ errors.first('password') }} {% endif %}
		</div>
		<div>
			<input type="password" name="password_confirm" placeholder="New Password Confirm">
			{% if errors.first('password_confirm') %}  {{ errors.first('password_confirm') }} {% endif %}
		</div>
		<input type="hidden" name="{{ csrf_key }}" id="submit" value="{{ csrf_token }}">
		<button name="passwordUpdate">Reset Password</button>
	</form>



{% endblock %}
