{%  extends 'templates/default.php' %}

{% block content %}

	<a href="{{ urlFor('users.users') }}"> << Back to Users List </a>


		<h3>You're Editing, {{ user.getFullNameOrUsername }}</h3>

		<h4><strong>Edit Profile Information</strong></h4>
	<form action="{{ urlFor('users.edit', {user: user.id}) }}" method="post">
		<div>
			<input type="text" name="firstname" placeholder="Update First Name" {% if request.post('firstname') %} value="{{ request.post('firstname') }}" {%else%}  value="{{ user.first_name }}" {%endif%}>
			{% if errors.first('firstname') %}  {{ errors.first('firstname') }} {% endif %}
		</div>
		<div>
			<input type="text" name="lastname" placeholder="Update Last Name" {% if request.post('lastname') %} value="{{ request.post('lastname') }}" {%else%}  value="{{ user.last_name }}" {%endif%}>
			{% if errors.first('lastname') %}  {{ errors.first('lastname') }} {% endif %}
		</div>
		<input type="hidden" name="{{ csrf_key }}" id="submit" value="{{ csrf_token }}">
		<button name="submit">Update Name</button>
	</form>




	<h4><strong>Reset Password</strong></h4>
	<form action="{{ urlFor('users.edit', {user: user.id}) }}" method="post">
		<div>
			<input type="password" name="password" placeholder="New Password">
			{% if errors.first('password') %}  {{ errors.first('password') }} {% endif %}
		</div>
		<div>
			<input type="password" name="password_confirm" placeholder="New Password Confirm">
			{% if errors.first('password_confirm') %}  {{ errors.first('password_confirm') }} {% endif %}
		</div>
		<input type="hidden" name="{{ csrf_key }}" id="submit" value="{{ csrf_token }}">
		<button name="passwordUpdate">Reset Password</button>
	</form>




	<h3><strong>Admin Levels</strong></h3>
	<form action="{{ urlFor('users.edit', {user: user.id}) }}" method="post">
		<input type="checkbox" name="makeAdmin" id="makeAdmin" {% if perms.is_admin is sameas(true) %} checked="" {%else%} checked="checked" {%endif%}>
		<label for="makeAdmin">Make this user an Admin(This does work)</label><br>
		<input type="hidden" name="{{ csrf_key }}" id="submit" value="{{ csrf_token }}">
		<button name="addAdmin">Make Admin</button>
	</form>



{% endblock %}
