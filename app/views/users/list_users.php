{%  extends 'templates/default.php' %}

{% block content %}


	<p><strong>{{ userCount }}</strong> active users!</p>

	<ul>
	{% for user in users %}
		<li><a href="{{ urlFor('users.edit', {user: user.id}) }}">{{ user.getFullName }}</a></li>
	{% endfor %}
	</ul>


{% endblock %}
