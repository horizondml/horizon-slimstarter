{% if auth %}
	<h4>Hello {{ auth.getFullNameOrUsername }},</h4>
{% else %}
	<h4>Hello there, </h4>
{% endif %}

{% block content %}{% endblock %}