{%  extends 'email/templates/default.php' %}


{% block content %}
	<p><strong>Looks like someone request A password reset</strong></p>
	<p><a href="{{baseURL}}{{urlFor('reset')}}?email={{user.email}}&ident={{identifier|url_encode}}">Recover Password</a></p>

	<p>Just ignore this email if it wasn't you!</p>

{% endblock %}