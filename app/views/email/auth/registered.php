{%  extends 'email/templates/default.php' %}


{% block content %}
	<p><strong>You've been successfully registered.</strong></p>
	<p><a href="{{baseURL}}{{urlFor('activate')}}?email={{user.email}}&ident={{identifier|url_encode}}">Activate this account</a></p>

{% endblock %}