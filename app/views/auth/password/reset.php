{%  extends 'templates/default.php' %}

{% block content %}

	<form action="{{ urlFor('reset.post') }}?email={{email}}&ident={{ident|url_encode}}" method="post" autocomplete="off">
		<div>
			<label for="password">New Password</label>
			<input type="password" name="password" id="password" autocomplete="off">
			{% if errors.first('password') %}
				{{ errors.first('password') }}
			{% endif %}
		</div>
		<div>
			<label for="passwordConfirm">New Password Confirm</label>
			<input type="password" name="password_confirm" id="passwordConfirm" autocomplete="off">
			{% if errors.first('password_confirm') %}
				{{ errors.first('password_confirm') }}
			{% endif %}
		</div>
		<div>
			<input type="hidden" name="{{ csrf_key }}" id="submit" value="{{ csrf_token }}">
			<input type="submit" type="submit" name="submit" id="submit" value="Update Password">
		</div>
	</form>


{% endblock %}
