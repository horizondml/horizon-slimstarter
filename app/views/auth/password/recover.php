{%  extends 'templates/default.php' %}

{% block content %}

	<form action="{{ urlFor('recover') }}" method="post" autocomplete="off">
		<div>
			<label for="email">Enter Email</label>
			<input type="text" name="email" id="email" {% if request.post('email') %} value="{{ request.post('email') }}" {%endif%}>
			{% if errors.first('email') %}
				{{ errors.first('email') }}
			{% endif %}
		</div>

		<div>
			<input type="hidden" name="{{ csrf_key }}" id="submit" value="{{ csrf_token }}">
			<input type="submit" type="submit" name="submit" id="submit" value="Reset">
		</div>
	</form>


{% endblock %}
