	{% extends 'templates/default.php' %}
	{% block content %}
	<form action="{{ urlFor('login.post') }}" method="post" autocomplete="off">
		<div>
			<label for="email">Email/Username</label>
			<input type="text" name="ident" id="ident">
			{% if errors.first('ident') %}
				{{ errors.first('ident') }}
			{% endif %}
		</div>
		<div>
			<label for="email">Password</label>
			<input type="password" name="password" id="password">
			{% if errors.first('password') %}
				{{ errors.first('password') }}
			{% endif %}
		</div>
		<div>
			<input type="checkbox" name="remember" id="remember"><label for="remember">Remember Me?</label>
		</div>
		<div>
			<input type="hidden" name="{{ csrf_key }}" id="submit" value="{{ csrf_token }}">
			<input type="submit" name="submit" id="submit" value="Login">
		</div>

		<a href="{{urlFor('recover')}}">Forgot Password?</a>
	</form>
	{% endblock %}