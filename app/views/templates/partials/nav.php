
<ul>
	<li><a href="{{ urlFor('home') }}">Home</a></li>
{% if auth %}
	{% if auth.admin() %}
		<li><a href="{{ urlFor('users.users')}}">Edit Users</a></li>
	{%endif%}
	<li><a href="{{ urlFor('users.edit.profile', {profile: auth.username}) }}">Edit Profile</a></li>
	<li><strong><a href="{{ urlFor('logout') }}">Logout</a></strong></li>

{% elseif not auth %}
	<li><a href="{{ urlFor('register') }}">Register</a></li>
	<li><a href="{{ urlFor('login') }}">Login</a></li>
{%endif%}
</ul>