<!DOCTYPE html>
<html>
	<head>
		<title>MyAuth | {%  block title %} {{title}} {%  endblock title %}</title>
	</head>
	<body>
		{% include 'templates/partials/messages.php' %}
		{% include 'templates/partials/nav.php' %}
		{%  block content %} {%  endblock content %}
	</body>
</html>
