<?php

namespace Tjrc\Validation;

use Violin\Violin;
use Tjrc\User\User;

class Validator extends Violin {

	public function __construct($user) {
		$this->user = $user;

		$this->addFieldMessages([
			'email'	=> [
				'uniqueEmail' => 'Email already exists!'
			],
			'username' => [
				'uniqueUsername' => 'Username already exists!'
			]
		]);

	}

	public function validate_uniqueEmail($value, $input, $args) {

		$user = $this->user->where('email', $value);

		if($user->count()) {
			return false;
		} else {
			return true;
		}
	}


	public function validate_uniqueUsername($value, $input, $args) {

		return ! (bool) $this->user->where('username', $value)->count();

	}


}
