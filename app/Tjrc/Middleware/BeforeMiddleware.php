<?php
namespace Tjrc\Middleware;

use Slim\Middleware;

class BeforeMiddleware extends Middleware{

    public function call(){

        $this->app->hook('slim.before', [$this, 'run']);

        $this->next->call();
    }

    public function run(){

        if(isset($_SESSION[$this->app->config->get('auth.session')])){

            $this->app->auth = $this->app->user->where('id', $_SESSION[$this->app->config->get('auth.session')])->first();
        }


		$this->checkRememberMe();


        $this->app->view()->appendData([
            'auth' => $this->app->auth,
            'baseURL' => $this->app->config->get('app.url'),
        ]);

    }

	protected function checkRememberMe() {

		if($this->app->getCookie($this->app->config->get('auth.remember')) && !$this->app->auth) {
			$data = $this->app->getCookie($this->app->config->get('auth.remember'));

			$creds = explode('___', $data);

			if(empty(trim($data)) || count($creds) !== 2) {
				$this->response->redirect($this->app->urlFor('home'));
			} else {

				$ident = $creds[0];
				$token = $this->app->hash->hash($creds[1]);

				$user = $this->app->user->where('remember_identifier', $ident)->first();

				if($user) {
					if($this->app->hash->hashCheck($ident, $user->remember_token)) {
						$_SESSION[$this->app->config->get('auth.session')] = $user->id;
						$this->app->auth = $this->app->user->where('id', $this->id)->first();
					} else {
						$user->removeRememberCreds();
					}
				}
			}

		}

	}

}