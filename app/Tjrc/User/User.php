<?php

/**
* User  Model
* @ Extends Eloquent Model
*/

namespace Tjrc\User;

use Illuminate\Database\Eloquent\Model as Eloquent;

class User extends Eloquent {

	protected $table = 'users';

	protected $fillable = [
		'email',
		'username',
		'password',
		'active',
		'active_hash',
		'remember_identifier',
		'remember_token'
	];

	public function getFullName() {
		if(!$this->first_name || !$this->last_name) {
			return null;
		}

		return "{$this->first_name} {$this->last_name}";

	}

	public function getFullNameOrUsername() {
		return $this->getFullName() ?: $this->username;
	}


	public function activateAccount() {

		$this->update([
			'active'		=> true,
			'active_hash'	=> null
		]);

	}


	public function updateRememberCreds($rememberIdent, $rememberToken) {
		$this->update([
			'remember_identifier' 	=> $rememberIdent,
			'remember_token'		=> $rememberToken
		]);
	}

	public function removeRememberCreds() {
		$this->updateRememberCreds(null, null);
	}

	public function hasPermissions($permissions) {
		return (bool) $this->permissions->{$permissions};
	}

	public function isAdmin() {
		return $this->hasPermissions('is_admin');
	}

	public function permissions() {
		return $this->hasOne('Tjrc\User\UserPermissions', 'user_id');
	}


}