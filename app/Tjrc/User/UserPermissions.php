<?php

namespace Tjrc\User;

use Illuminate\Database\Eloquent\Model as Eloquent;

class UserPermissions extends Eloquent {

	protected $table = 'user_permissions';

	protected $fillable = [
		'is_admin'
	];

	public static $defaults = [
		'is_admin'	=> false
	];

	public static $administrator = [
		'is_admin'	=> true
	];

}