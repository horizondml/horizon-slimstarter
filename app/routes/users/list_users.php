<?php
$app->get('/u/users', $admin(), function() use ($app){

	$users = $app->user->where('active', true)->get();

	$userCount  = count($users);

	$app->render('users/list_users.php', [
		'title'		=> 'All Users',
		'users' 	=> $users,
		'userCount'	=> $userCount
	]);

})->name('users.users');


