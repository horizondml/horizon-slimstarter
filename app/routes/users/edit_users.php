<?php
$app->get('/u/:user', $authenticated(), function($user) use ($app){

	$userid = $app->user->where('id', $user)->first();
	$perms = $app->userpermissions->where('user_id', $user)->get()->first();


	$app->render('users/edit_users.php', [
		'title'		=> "Editing User " . "{$userid->first_name} {$userid->last_name}",
		'user' 		=> $userid,
		'perms'		=> $perms,
	]);

})->name('users.edit');



$app->post('/u/:user', function($user) use ($app){


		$checked = "";

		$perms = $app->userpermissions->where('user_id', $user)->get()->first();
		$request = $app->request;
		//Name info
		$submit				= $request->post('submit');
		$firstname 			= $request->post('firstname');
		$lastname 			= $request->post('lastname');

		//Password
		$passUpdate			= $request->post('passwordUpdate');
		$password 			= $request->post('password');
		$password_confirm 	= $request->post('password_confirm');

		//Make Admin
		$addAdmin			= $request->post('addAdmin');
		$makeAdmin			= $request->post('makeAdmin');

		//Deactivate User
		$deactivateUser		= $request->post('deactivateUser');
		$deactivate			= $request->post('deactivate');


		$vi = $app->validation;




		/*
		*	Profile Information
		*/
		if(isset($submit)) {

			$vi->validate([
				'firstname|Your First Name'		=> [$firstname, 'required|alpha'],
				'lastname|Your Password'		=> [$lastname, 'required|alpha']
			]);


			if($vi->passes()) {

				$app->user->where('id', $app->auth->id)->update([
					'first_name'	=> $firstname,
					'last_name'		=> $lastname
				]);

				$app->flash('global', 'Success, profile information has been updated!');
				$app->response->redirect($user);

			}
		}


		/*
		*	Password Reset
		*/
		if(isset($passUpdate)) {

			$vi->validate([
				'password|Your New Password'		=> [$password, 'required|min(6)'],
				'password_confirm|Your Password'	=> [$password_confirm, 'required|matches(password)']
			]);

			if($vi->passes()) {

				$app->user->where('id', $app->auth->id)->update([
					'password'	=> $app->hash->password($password_confirm)
				]);

				$app->flash('global', 'Success, Your Password been reset!');
				$app->response->redirect($user);
			}

		}



		/*
		*	Make User Admin
		*/
		if(isset($addAdmin)) {

			if(isset($makeAdmin)) {

			 	$app->userpermissions->where('user_id', $user)->update([
						'is_admin'		=> 1,
			 	]);

			 	$checked = "checked";

				$app->flash('global', 'Success, this User is now an Admin!');
				$app->response->redirect($user);

			}

			if(!isset($makeAdmin)) {

			 	$app->userpermissions->where('user_id', $user)->update([
						'is_admin'		=> 0,
			 	]);

			 	$checked = "";

				$app->flash('global', 'This user is now a Basic User!');
				$app->response->redirect($user);
			}

		}




		/*
		*	Deactivate user Account
		*/
		if(isset($deactivateUser)) {

			if(isset($deactivate)) {

				$app->user->where('id', $app->user->id)->update([
					'active' => 0,
				]);

				$app->flash('global', 'This user has been deactivated and can no longer login!');
				$app->response->redirect($app->urlFor('users.edit'));
			}

			if(!isset($deactivate)) {

				$app->user->where('id', $app->user->id)->update([
					'active' => 1,
				]);

				$app->flash('global', 'Success! This user has been activated!');
				$app->response->redirect($app->urlFor('users.edit'));
			}

		}


		$app->render('users/edit_users.php', [
			'title'		=> 'Edit Users',
			'errors'	=> $vi->errors(),
			'user'		=> $user,
			'request'	=> $request,
			'perms'		=> $perms,
			'checked'	=> $checked
		]);




})->name('users.edit.post');


