<?php


	$app->get('/u/edit/:profile', function() use ($app) {

		$app->render('users/edit_profile.php',[
			'title'	=> 'Edit My Profile'
		]);

	})->name('users.edit.profile');



	$app->post('/u/edit/:profile', function() use ($app) {


		$request = $app->request;


		$submit			= $request->post('submit');
		$firstname 		= $request->post('firstname');
		$lastname 		= $request->post('lastname');


		$passUpdate			= $request->post('passwordUpdate');
		$password 			= $request->post('password');
		$password_confirm 	= $request->post('password_confirm');


		$vi = $app->validation;



		if(isset($submit)) {
			$vi->validate([
				'firstname|Your First Name'		=> [$firstname, 'required|alpha'],
				'lastname|Your Password'		=> [$lastname, 'required|alpha']
			]);


			if($vi->passes()) {

				$app->user->where('id', $app->auth->id)->update([
					'first_name'	=> $firstname,
					'last_name'		=> $lastname
				]);

				$app->flash('global', 'Success, Your profile information has been updated!');
				$app->response->redirect($app->urlFor('users.edit.profile'));

			}
		}



		if(isset($passUpdate)) {



			$vi->validate([
				'password|Your New Password'		=> [$password, 'required|min(6)'],
				'password_confirm|Your Password'	=> [$password_confirm, 'required|matches(password)']
			]);

			if($vi->passes()) {

				$app->user->where('id', $app->auth->id)->update([
					'password'	=> $app->hash->password($password_confirm)
				]);
				$app->flash('global', 'Success, Your Password been reset!');
				$app->response->redirect($app->urlFor('users.edit.profile'));
			}

		}


		$app->render('users/edit_profile.php', [
			'errors'	=> $vi->errors(),
			'request'	=> $request
		]);



	})->name('users.edit.profile.post');







/*

	$app->get('/u/edit/:password', function() use ($app) {

		$app->render('users/edit_profile.php',[
			'title'	=> 'Edit My Profile'
		]);

	})->name('users.edit.password');



	$app->post('/u/edit/:password', function() use ($app) {



	})->name('users.edit.password.post');
*/




