<?php
$app->get('/dashboard', $authenticated(), function() use ($app){


	$app->render('dashboard/dashboard.php', [
		'title'	=> 'Dashboard'
	]);

})->name('dashboard');


