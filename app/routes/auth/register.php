<?php

	use Tjrc\User\UserPermissions;

	$app->get('/register', function() use ($app) {

		$app->render('auth/register.php',[
			'title'	=> 'Login'
		]);

	})->name('register');


	$app->post('/register', function() use ($app){

		$request = $app->request;

		$email 			= $request->post('email');
		$username		= $request->post('username');
		$password 		= $request->post('password');
		$pass_confirm 	= $request->post('password_confirm');


		$vi = $app->validation;

		$vi->validate([
			'email' 			=> [$email, 'required|email|uniqueEmail'],
			'username' 			=> [$username, 'required|alnumDash|max(20)|uniqueUsername'],
			'password' 			=> [$password, 'required|min(6)'],
			'password_confirm' 	=> [$pass_confirm, 'required|matches(password)']
		]);

		if($vi->passes()) {

			$identifier = $app->randomlib->generateString(128);

			//insert into database
			$user = $app->user->create([
				'email' 		=> $email,
				'username' 		=> $username,
				'password' 		=> $app->hash->password($password),
				'active'		=> false,
				'active_hash'	=> $app->hash->hash($identifier),
			]);


			$user->permissions()->create(UserPermissions::$defaults);


			//get the view to get data we want to send to new user
			$app->mail->send('email/auth/registered.php', ['user' => $user, 'identifier' => $identifier], function($message) use ($user) {
				$message->to($user->email);
				$message->subject('Thank you for registering');
			});


			//create app flash message
			$app->flash('global', 'You have been registered!');

			//redirect to home
			$app->response->redirect($app->urlFor('login'));


		} else {

			$app->render('auth/register.php', [
				'title'		=> 'Register',
				'errors' 	=> $vi->errors(),
				'request'	=> $request,
			]);

		}


	})->name('register.post');