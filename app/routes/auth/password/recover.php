<?php
$app->get('/recover', $guest(), function() use ($app) {

	$app->render('auth/password/recover.php', [
		'title'	=>  'Recover Password'
	]);

})->name('recover');


$app->post('/recover', $guest, function() use ($app) {

	$request = $app->request();

	$email = $request->post('email');

	$user = $app->user->where('email', $email)->where('active', true)->first();


	$vi = $app->validation;

	$vi->validate([
		'email' 			=> [$email, 'required|email'],
	]);

	if($vi->passes()) {


		if(!$user) {

			$app->flash('global', 'Sorry we could not find ' . $email . ' in our records');
			$app->redirect('recover');

		} else {

			$identifier = $app->randomlib->generateString(128);

			$app->user->where('email', $email)->update([
				'recover_hash'	=> $app->hash->hash($identifier)
			]);

			$app->mail->send('email/auth/recover_pass.php', ['user' => $user, 'identifier' => $identifier], function($message) use ($user) {
				$message->to($user->email);
				$message->subject('Your Password Recovery');
			});

			$app->flash('global', 'Please check ' . $user->email . ' for password recovery.');
			$app->redirect('recover');

		}


	}

	$app->render('auth/password/recover.php', [
		'title'		=> 'Recover Password',
		'errors'	=> $vi->errors(),
		'request'	=> $request
	]);




})->name('recover.post');