<?php
$app->get('/reset', $guest(), function() use ($app) {


	$request = $app->request();

	$email = $request->get('email');
	$ident = $request->get('ident');

	$user = $app->user->where('email', $email)->where('active', true)->first();

	$hashedIdent  = $app->hash->hash($ident);


	if(!$user) {
		return $app->response->redirect($app->urlFor('recover'));
	}

	if(!$user->recover_hash) {
		return $app->response->redirect($app->urlFor('recover'));
	}
	if(!$app->hash->hashCheck($user->recover_hash, $hashedIdent)) {
		return $app->response->redirect($app->urlFor('recover'));
	}


	$app->render('auth/password/reset.php', [
		'title' => 'Reset Password',
		'email'	=> $user->email,
		'ident'	=> $ident
	]);



})->name('reset');


$app->post('/reset', $guest, function() use($app){

	$request = $app->request();


	//get
	$ident 				= $request->get('ident');
	$email 				= $request->get('email');

	//post
	$password 			= $request->post('password');
	$passwordConfirm 	= $request->post('password_confirm');


	$user = $app->user->where('email', $email)->where('active', true)->first();

	$hashedIdent  = $app->hash->hash($ident);

	if(!$user) {
		return $app->response->redirect($app->urlFor('recover'));
	}

	if(!$user->recover_hash) {
		return $app->response->redirect($app->urlFor('recover'));
	}
	if(!$app->hash->hashCheck($user->recover_hash, $hashedIdent)) {
		return $app->response->redirect($app->urlFor('recover'));
	}

	$vi = $app->validation;

	$vi->validate([
		'password|Password' 						=> [$password, 'required|min(6)'],
		'password_confirm|Password Confirmation' 	=> [$passwordConfirm, 'required|matches(password)'],
	]);

	if($vi->passes()) {

		//update new password and recover hash
		$app->user->update([
			'password'		=> $app->hash->password($password),
			'recover_hash'	=> null
		]);

		$app->flash('global', 'Password has been reset for ' . $user->email . ', You may now login!');
		$app->response->redirect($app->urlFor('login'));
	}

	$app->render('auth/password/reset.php', [
		'title'		=> 'Reset Password',
		'errors'	=> $vi->errors(),
		'request'	=> $request,
		'email'	=> $user->email,
		'ident'	=> $ident
	]);


})->name('reset.post');