<?php

use Carbon\Carbon;

$app->get('/login', $guest(), function() use ($app) {

	$app->render('auth/login.php', [
		'title'	=> 'Login'
	]);

})->name('login');


$app->post('/login', $guest(), function() use ($app) {

	$request = $app->request;

	$ident 			= $request->post('ident');
	$password 		= $request->post('password');
	$remember		= $request->post('remember');

	$vi = $app->validation;

	$vi->validate([
		'ident|Your Username'		=> [$ident, 'required'],
		'password|Your Password'	=> [$password, 'required']
	]);



	if($vi->passes()) {


		$user = $app->user->where('active', true)->where(function($query) use ($ident){
			return $query->where('email', $ident)->orWhere('username', $ident);
		})->first();


		if($user && $app->hash->password_check($password, $user->password)) {
			$_SESSION[$app->config->get('auth.session')] = $user->id;

			if($remember == 'on') {
				$rememberIdent  = $app->randomlib->generateString(128);
				$rememberToken  = $app->randomlib->generateString(128);

				$user->updateRememberCreds($rememberIdent, $app->hash->hash($rememberToken));

				$app->setCookie($app->config->get('auth.remember'),
					"{$rememberIdent}___{$rememberToken}",
					Carbon::parse('+1 week')->timestamp

				);
			}

			$app->flash('global', 'Successfully logged in!');

			$app->response->redirect($app->urlFor('dashboard'));

		} else {

			$app->flash('global', 'Sorry username or password is incorrect!');
			$app->response->redirect($app->urlFor('login'));

		}
	}
		$app->render('auth/login.php', [
			'title'		=> 'Login',
			'errors' 	=> $vi->errors(),
			'request'	=> $request,
		]);


})->name('login.post');