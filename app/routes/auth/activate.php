<?php
$app->get('/activate', $guest(), function() use ($app) {

	$request = $app->request;

	$email = $request->get('email');
	$ident = $request->get('ident');

	$user = $app->user->where('email', $email)->where('active', false)->first();

	$hashedIdent  = $app->hash->hash($ident);


	if(!$user || !$app->hash->hashCheck($user->active_hash, $hashedIdent)) {
		$app->flash('global', 'Sorry there was a problem acivating your account');
		$app->response->redirect($app->urlFor('register'));
	} else {
		$user->activateAccount();

		$app->flash('global', 'Great! Your account is now active and you may login.');
		$app->response->redirect($app->urlFor('login'));
	}



})->name('activate');