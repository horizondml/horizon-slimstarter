<?php
	$app->get('/logout', $authenticated(), function() use ($app) {

		unset($_SESSION[$app->config->get('auth.session')]);

		if($app->getCookie($app->config->get('auth.remember'))) {
			$app->auth->removeRememberCreds();
			$app->deleteCookie($app->config->get('auth.remember'));
		}

		$app->flash('global', 'You\'re now logged out!');
		$app->response->redirect($app->urlFor('login'));
	})->name('logout');
