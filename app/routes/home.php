<?php
$app->get('/', function() use ($app) {

	//Redirect to login if not loggedin


	if(!isset($_SESSION[$app->config->get('auth.session')])){
		$app->response->redirect($app->urlFor('login'));
	} else {
		$app->response->redirect($app->urlFor('dashboard'));
	}

	$app->render('home.php', array(
		"title" => "Dashboard",
	));

})->name('home');
