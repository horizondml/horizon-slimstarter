<?php

$authCheck = function($required) use ($app) {
	return function() use($required, $app){
		if((!$app->auth && $required) || ($app->auth && !$required)) {
			$app->redirect($app->urlFor('home'));
		}
	};
};

$authenticated = function() use ($authCheck) {
	return $authCheck(true);
};

$guest = function() use($authCheck) {
	return $authCheck(false);
};



$admin = function() use ($app) {
	return function() use($app) {
		if(!$app->auth || !$app->auth->isAdmin()) {
			$app->redirect($app->urlFor('dashboard'));
		}
	};
};